<?php
// src/App/DataFixtures/ORM/LoadCategory.php
namespace App\DataFixtures\ORM;

//use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Skill;

class LoadSkill extends  Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        // Liste des noms de compétences à ajouter
        $names = array('PHP', 'Symfony', 'C++', 'Java', 'Photoshop', 'Blender', 'Bloc-note');

        foreach ($names as $name) {
        // On crée la compétence
        $skill = new Skill();
        $skill->setName($name);

        // On la persiste
        $manager->persist($skill);
        }

        // On déclenche l'enregistrement de toutes les catégories
        $manager->flush();
    }

    public function getDependencies()
    {
        return array(
            LoadCategory::class,
        );
    }
}
