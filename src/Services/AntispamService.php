<?php
// src/Services/ComplexObject.php
namespace App\Services;

class AntispamService
{
  private $mailer;
  // private $locale;
  private $minLength;

  public function __construct(\Swift_Mailer $mailer, $minLength)
  {
    $this->mailer    = $mailer;
    // $this->locale    = $locale;
    $this->minLength = $minLength;
  }

  /**
   * Vérifie si le texte est un spam ou non
   *
   * @param string $text
   * @return bool
   */

  public function isSpam($text)
  {
    return strlen($text) < $this->minLength;
    // return strlen($text) < 50;
  }
}
