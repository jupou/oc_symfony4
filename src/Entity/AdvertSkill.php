<?php
// src/Entity/AdvertSkill.php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="oc_advert_skill")
 */
class AdvertSkill
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="level", type="string", length=255)
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Advert")
     * @ORM\JoinColumn(nullable=false)
     */
    private $advert;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Skill")
     * @ORM\JoinColumn(nullable=false)
     */
    private $skill;

    // ... vous pouvez ajouter d'autres attributs bien sûr


    public function getId()
    {
        return $this->id;
    }

    public function setLevel($level)
    {
        $this->level = $level;
    }

    public function getLevel()
    {
        return $this->level;
    }

    public function setAdvert(Advert $advert)
    {
        $this->advert = $advert;
    }

    public function getAdvert()
    {
        return $this->advert;
    }

    public function setSkill(Skill $skill)
    {
        $this->skill = $skill;
    }

    public function getSkill()
    {
        return $this->skill;
    }

}