<?php
// src/Controller/HomePageController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Annotation\Route;

class HomePageController extends AbstractController
{
  /**
   * @Route("/", name="platform_index")
   */
  public function homeAction()
    {
      return $this->render('homepagelayout.html.twig');
    }

  /**
   * @Route("index.php/contact", name="platform_contact")
   */
  public function contactAction()
    {
      // $url = $this->generateUrl('platform_index');
      // $this->addFlash('notice', "La page de contact n'est pas encore disponible, merci de revenir plus tard");

      // return new RedirectResponse($url);
      return $this->redirectToRoute('platform_index');
    }
}
