<?php
// src/Controller/AdvertController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\FormRenderer;

// use Symfony\Component\Form\FormBuilderInterface;

use App\Services\AntispamService;
use App\Entity\Advert;
use App\Entity\Application;
use App\Entity\Image;
// use App\Service\Antispam;

/**
 * @Route("/advert")
 */
class AdvertController extends AbstractController
{
  /**
   * @Route("/{page}", name="oc_advert_index", requirements={"page" = "\d+"}, defaults={"page" = 1})
   */
  public function indexAction($page)
    {
      // Notre liste d'annonce en dur
      $listAdverts = array(
        array(
          'title'   => 'Recherche développpeur Symfony',
          'id'      => 38,
          'author'  => 'Alexandre',
          'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
          'date'    => new \Datetime()),
        array(
          'title'   => 'Mission de webmaster',
          'id'      => 39,
          'author'  => 'Hugo',
          'content' => 'Nous recherchons un webmaster capable de maintenir notre site internet. Blabla…',
          'date'    => new \Datetime()),
        array(
          'title'   => 'Offre de stage webdesigner',
          'id'      => 40,
          'author'  => 'Mathieu',
          'content' => 'Nous proposons un poste pour webdesigner. Blabla…',
          'date'    => new \Datetime())
      );

      // Et modifiez le 2nd argument pour injecter notre liste
      return $this->render('Advert/index.html.twig', array('listAdverts' => $listAdverts
      ));
    }

  public function menuAction($limit)
    {
      // On fixe en dur une liste ici, bien entendu par la suite
      // on la récupérera depuis la BDD !
      $listAdverts = array(
        array('id' => 2, 'title' => 'Recherche développeur Symfony'),
        array('id' => 5, 'title' => 'Mission de webmaster'),
        array('id' => 9, 'title' => 'Offre de stage webdesigner')
      );

      return $this->render('Advert/menu.html.twig', array(
        // Tout l'intérêt est ici : le contrôleur passe
        // les variables nécessaires au template !
        'listAdverts' => $listAdverts
      ));
    }

  /**
   * @Route("/view/{id}", name="oc_advert_view", requirements={"id" = "\d+"})
   */
  public function view($id)
  {
    // Ici, on récupérera l'annonce correspondante à l'id $id

    // On récupère le repository
        $em = $this->getDoctrine()
          ->getManager()
          ->getRepository('App:Advert')
        ;

        // On récupère l'entité correspondante à l'id $id
        $advert = $em->find($id);

        // $advert est donc une instance de OC\PlatformBundle\Entity\Advert
        // ou null si l'id $id  n'existe pas, d'où ce if :
        if (null === $advert) {
          throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
        }

      // On avait déjà récupéré la liste des candidatures
      $listApplications = $this->getDoctrine()
          ->getRepository('App:Application')
          ->findBy(array('advert' => $advert))
      ;

      // On récupère maintenant la liste des AdvertSkill
      $listAdvertSkills = $this->getDoctrine()
          ->getRepository('App:AdvertSkill')
          ->findBy(array('advert' => $advert))
      ;

    return $this->render('Advert/view.html.twig', array(
      'advert' => $advert,
        'listApplications' => $listApplications,
      'listAdvertSkills' => $listAdvertSkills
    ));
  }

  /**
   * @Route("/add", name="oc_advert_add")
   */
  public function add(Request $request, AntispamService $antispam)
  {
      // On crée un objet Advert
      $advert = new Advert();

      // On crée le FormBuilder grâce au service form factory
//      $formBuilder = $this->get('form.factory')->createBuilder(FormType::class, $advert);
      // $formFactory = Forms::createFormFactoryBuilder()
      //     ->getFormFactory();
      // On ajoute les champs de l'entité que l'on veut à notre formulaire
      $form = $this->createForm(FormType::class,$advert);
      // $form = $formFactory->createBuilder(FormType::class,$advert);
      $form
          ->add('date',      DateType::class)
          ->add('title',     TextType::class)
          ->add('content',   TextareaType::class)
          ->add('author',    TextType::class)
          ->add('published', CheckboxType::class)
          ->add('save',      SubmitType::class)
          // ->getForm();
      ;
      // $form->getForm();
      // Pour l'instant, pas de candidatures, catégories, etc., on les gérera plus tard


      // On passe la méthode createView() du formulaire à la vue
      // afin qu'elle puisse afficher le formulaire toute seule
      return $this->render('Advert/add.html.twig', array(
          'form' => $form->createView(),
      ));
  }

  /**
   * @Route("/edit/{id}", name="oc_advert_edit", requirements={"id" = "\d+"})
   */
  public function edit($id, Request $request)
  {
//    $advert = array(
//      'title'   => 'Recherche développpeur Symfony',
//      'id'      => $id,
//      'author'  => 'Alexandre',
//      'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
//      'date'    => new \Datetime()
//    );

    $em = $this->getDoctrine()->getManager();

    // On récupère l'annonce $id
    $advert = $em->getRepository('App:Advert')->find($id);

    if (null === $advert) {
      throw new NotFoundHttpException("L'annonce d'id ".$id." n'existe pas.");
    }

    // La méthode findAll retourne toutes les catégories de la base de données
    $listCategories = $em->getRepository('App:Category')->findAll();

    // On boucle sur les catégories pour les lier à l'annonce
    foreach ($listCategories as $category) {
      $advert->addCategory($category);
    }

    // Pour persister le changement dans la relation, il faut persister l'entité propriétaire
    // Ici, Advert est le propriétaire, donc inutile de la persister car on l'a récupérée depuis Doctrine

    // Étape 2 : On déclenche l'enregistrement
    $em->flush();

    return $this->render('Advert/edit.html.twig', array(
      'advert' => $advert
    ));
  }

  /**
   * @Route("/delete/{id}", name="oc_advert_delete", requirements={"id" = "\d+"})
   */
  public function delete($id)
  {
    // Ici, on récupérera l'annonce correspondant à $id
    $advert = array(
      'title'   => 'Recherche développpeur Symfony',
      'id'      => $id,
      'author'  => 'Alexandre',
      'content' => 'Nous recherchons un développeur Symfony débutant sur Lyon. Blabla…',
      'date'    => new \Datetime()
    );

    return $this->render('Advert/delete.html.twig', array(
      'advert' => $advert
    ));

    // Ici, on gérera la suppression de l'annonce en question
  }

    //Edit image
    /**
     * @Route("/editimage/{id}", name="oc_advert_edit_image", requirements={"id" = "\d+"})
     */
    public function editImageAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        // On récupère l'annonce
        $advert = $em->getRepository('App:Advert')->find($id);

        // On modifie l'URL de l'image par exemple
        $advert->getImage()->setUrl('https://interactive-examples.mdn.mozilla.net/media/examples/grapefruit-slice-332-332.jpg');

        // On n'a pas besoin de persister l'annonce ni l'image.
        // Rappelez-vous, ces entités sont automatiquement persistées car
        // on les a récupérées depuis Doctrine lui-même

        // On déclenche la modification
        $em->flush();

        return new Response('OK');
    }

}
